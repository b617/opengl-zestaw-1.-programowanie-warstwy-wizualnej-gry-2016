#include "stdafx.h"
/*Standard libraries*/
#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <math.h>

/*Additional libraries*/
#include "gl3w.h"
#include <GLFW\glfw3.h>

/*Classes*/
#include "Shader.h"
#include "Mesh.h"
#include "Application.h"

//---------------------------------------------------------
#define M_PI 3.14159265358979323846
void StartGL();
void QuitGL();



Mesh * WielokatForemny(unsigned int numVertices, float radius){
	float angle = M_PI * 2 / numVertices, currentAngle = 0.0f;
	Vertex * vertices = (Vertex*)malloc(sizeof(Vertex)*numVertices);

	for (unsigned int i = 0; i < numVertices; i++, currentAngle += angle) {
		vertices[i] = Vertex(glm::vec3(radius*cos(currentAngle), radius*sin(currentAngle), 0.0f));
		std::cout << "Vertex " << i << ": " << vertices[i].toString() << '\n';
	}
	
	return new Mesh(vertices, numVertices, GL_TRIANGLE_FAN);
}

bool setValue(float & floatToSet, const char* message){
	std::cout << message;
	std::cin >> floatToSet;
	return (floatToSet >= 0.0f && floatToSet <= 1.0f);
}

int main(int argc, char* argv[])
{
	/* INIT */
	StartGL();
	Application & application = Application::GetInstance();

	///////////////////////////
	float r, g, b;
	while (!setValue(r, "Set value for color RED (range [0;1]): "));
	while (!setValue(g, "Set value for color GREEN (range [0;1]): "));
	while (!setValue(b, "Set value for color BLUE (range [0;1]): "));
	float radius;
	unsigned int numVertices = 0;
	while (!setValue(radius, "Set size of your polygon (range [0;1]): "));
	while (numVertices < 3 || numVertices > 100) {
		std::cout << "Set number of vertices of your polygon (range {3;100}): ";
		std::cin >> numVertices;
	}
	//////////////////////////

	Shader shader("shaders/myShader");
		GLint shader_colorPosition = glGetUniformLocation(shader.GetProgramID(), "inColor");
		GLint shader_positionPosition = glGetAttribLocation(shader.GetProgramID(), "position");


	Mesh * mesh = WielokatForemny(numVertices,radius);

	/* MAIN LOOP */
	while (glfwWindowShouldClose(application.window) == GL_FALSE) {
		glfwGetFramebufferSize(application.window, &(application.windowSizeX), &(application.windowSizeY));
		application.windowRatio = 1.0f * application.windowSizeX / application.windowSizeY;
		glViewport(0, 0, application.windowSizeX, application.windowSizeY);
		glClear(GL_COLOR_BUFFER_BIT);

		////////////////////////////////
		glUniform3f(shader_colorPosition, r, g, b);
		shader.Bind();
		mesh->Draw();
		//mesh2.Draw();
		///////////////////////////////

		glfwSwapBuffers(application.window);
		glfwPollEvents();
	}

	QuitGL();
	return EXIT_SUCCESS;
}

void StartGL(){
	/*GLFW*/
	if (glfwInit() == false) {
		puts("Failed to initialize glfw");
		system("pause");
		exit(EXIT_FAILURE);
	}

	Application & application = Application::GetInstance();
	if (application.CreateNewWindow() == false) {
		puts("Failed to create window");
		glfwTerminate();
		system("Pause");
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(application.window);
	glfwSetKeyCallback(application.window, key_callback);
	glfwSwapInterval(1);

	/*GL3W*/
	int iTemp = gl3wInit();
	puts("openGL version:");
	const GLubyte* version = glGetString(GL_VERSION);
	printf("%s\n", version);

	if (iTemp != 0){
		puts("Failed to initialize gl3w: ");
		fprintf(stderr, std::to_string(iTemp).c_str());
		system("pause");
		exit(EXIT_FAILURE);
	}

	glClearColor(0, 0, 0, 1);
}
void QuitGL(){
	glfwDestroyWindow(Application::GetInstance().window);
	glfwTerminate();
}