#pragma once
#include "glm\glm.hpp"
#include "gl3w.h"
#include <string>

class Vertex;

class Mesh
{
public:
	Mesh(Vertex* vertices, unsigned int numVertices, GLuint drawingMode = GL_TRIANGLES);

	void Draw();

	Mesh & setDrawingMode(GLuint value);

	virtual ~Mesh();
private:
	enum
	{
		POSITION_VB,

		NUM_BUFFERS
	};
	GLuint mVertexArrayObject;
	GLuint mVertexArrayBuffers[NUM_BUFFERS];
	unsigned int mDrawCount;
	GLuint mDrawingMode;
};

class Vertex{
public:
	Vertex(const glm::vec3& position) : pos(position){ }
	std::string toString(){
		return std::to_string(pos.x) + ", " + std::to_string(pos.y) + ", " + std::to_string(pos.z);
	}

protected:
	glm::vec3 pos;
};
