#version 140
 
uniform vec3 inColor;
attribute vec3 position;

void main()
{
    gl_Position = vec4(position,1.0);
}