#pragma once
#include <vector>
#include <GLFW\glfw3.h>
#include "Sprite.h"
class Application{
private:
	Application(){
	}
	~Application(){
		for each (Sprite* s in AllSprites)
		{
			s->~Sprite();
		}
		AllSprites.clear();
	}
public:
	static Application & GetInstance(){
		static Application instance;
		return instance;
	}
	Application(Application const&) = delete;
	void operator=(Application const&) = delete;

	GLFWwindow * window;
	int windowSizeX = 800, windowSizeY = 600;
	float windowRatio = 1.0f * windowSizeX / windowSizeY;
	std::string windowTitle = "OpenGL window with GLFW, gl3w and glm";
	bool CreateNewWindow(){

		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

		glfwWindowHint(GLFW_OPENGL_ANY_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		window = glfwCreateWindow(windowSizeX, windowSizeY, windowTitle.c_str(), NULL, NULL);
		return (window != NULL);
	}

	std::vector<Sprite*> AllSprites;
	void AddSprite(Sprite * const s){
		AllSprites.push_back(s);
	}
	static void Test(){}

};

static void error_callback(int error, const char* description){
	fputs(description, stderr);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//std::cout << key << " " << scancode << " " << action << " " << mods <<std::endl;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

