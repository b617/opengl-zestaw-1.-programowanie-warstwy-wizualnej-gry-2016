#include "stdafx.h"
/*Standard libraries*/
#include <stdlib.h>
#include <string>
#include <vector>

/*Additional libraries*/
#include "gl3w.h"
#include <GLFW\glfw3.h>

/*Classes*/
#include "Shader.h"
#include "Mesh.h"
#include "Application.h"

//MAKRO PARSE_TAB(nazwa_tablicy): zwraca nazwe tablicy i jej rozmiar po przecinku
#define PARSE_TAB(X)		X, sizeof(X) / sizeof(X[0])

//---------------------------------------------------------
void StartGL();
void QuitGL();

glm::vec3 SrodekMasy(Vertex* vertices, unsigned int vericesCount) {
	float x = 0.0f, y = 0.0f;
	for (unsigned int i = 0; i < vericesCount; i++){
		x += vertices[i].getPosition().x;
		y += vertices[i].getPosition().y;
	}
	return glm::vec3(x / vericesCount, y / vericesCount, 0.0f);
}

float NajwiekszaOdleglosc(glm::vec3 srodekMasy, Vertex* vertices, unsigned int vericesCount){
	float maxDistance = 0.0f;
	for (unsigned int i = 0; i < vericesCount; i++){
		float distance = glm::distance(srodekMasy, vertices[i].getPosition());
		if (distance>maxDistance) maxDistance = distance;
	}
	return maxDistance;
}

int main(int argc, char* argv[])
{
	/* INIT */
	StartGL();
	Application & application = Application::GetInstance();

	Shader shader("shaders/testShader");
	GLuint shader_pozycjaCenterOfMass = glGetAttribLocation(shader.GetProgramID(), "centerOfMass");
	GLuint shader_pozycjaMaxDistance = glGetAttribLocation(shader.GetProgramID(), "maxDistance");

	Vertex vertices[] = { Vertex(glm::vec3(-.5, -.5, 0)), Vertex(glm::vec3(0, .5, 0)), Vertex(glm::vec3(.5, -.5, 0)) };
	Mesh mesh(PARSE_TAB(vertices));
	Vertex vertices2[] = { Vertex(glm::vec3(-.3, .3, 0)), Vertex(glm::vec3(0, .4, 0)), Vertex(glm::vec3(.5, .3, 0)), Vertex(glm::vec3(0, -.85, 0)) };
	Mesh mesh2(PARSE_TAB(vertices2), GL_TRIANGLE_FAN);

	glm::vec3 srodekMasy = SrodekMasy(PARSE_TAB(vertices2));
	float najwiekszaOdleglosc = NajwiekszaOdleglosc(srodekMasy, PARSE_TAB(vertices2));
	glVertexAttrib3f(shader_pozycjaCenterOfMass, srodekMasy.x, srodekMasy.y, srodekMasy.z);
	glVertexAttrib1f(shader_pozycjaMaxDistance, najwiekszaOdleglosc);


	/* MAIN LOOP */
	while (glfwWindowShouldClose(application.window) == GL_FALSE) {
		glfwGetFramebufferSize(application.window, &(application.windowSizeX), &(application.windowSizeY));
		application.windowRatio = 1.0f * application.windowSizeX / application.windowSizeY;
		glViewport(0, 0, application.windowSizeX, application.windowSizeY);
		glClear(GL_COLOR_BUFFER_BIT);

		////////////////////////////////
		shader.Bind();
		mesh2.Draw();
		///////////////////////////////

		glfwSwapBuffers(application.window);
		glfwPollEvents();
	}

	QuitGL();
	return EXIT_SUCCESS;
}

void StartGL(){
	/*GLFW*/
	if (glfwInit() == false) {
		puts("Failed to initialize glfw");
		system("pause");
		exit(EXIT_FAILURE);
	}

	Application & application = Application::GetInstance();
	if (application.CreateNewWindow() == false) {
		puts("Failed to create window");
		glfwTerminate();
		system("Pause");
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(application.window);
	glfwSetKeyCallback(application.window, key_callback);
	glfwSwapInterval(1);

	/*GL3W*/
	int iTemp = gl3wInit();
	puts("openGL version:");
	const GLubyte* version = glGetString(GL_VERSION);
	printf("%s\n", version);

	if (iTemp != 0){
		puts("Failed to initialize gl3w: ");
		fprintf(stderr, std::to_string(iTemp).c_str());
		system("pause");
		exit(EXIT_FAILURE);
	}

	glClearColor(0, 0, 0, 1);
}
void QuitGL(){
	glfwDestroyWindow(Application::GetInstance().window);
	glfwTerminate();
}