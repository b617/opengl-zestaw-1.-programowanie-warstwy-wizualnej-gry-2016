#version 150
 
attribute vec3 position;
attribute vec3 centerOfMass;
attribute float maxDistance;

out vec3 vCenterOfMass;
out float vMaxDistance;
out vec3 vPosition;

void main()
{
    gl_Position = vec4(position,1.0);
	vPosition=position;
	vMaxDistance=maxDistance;
	vCenterOfMass=centerOfMass;
}