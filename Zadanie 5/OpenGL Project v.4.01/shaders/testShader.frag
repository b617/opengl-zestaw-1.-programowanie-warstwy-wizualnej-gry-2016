#version 150
 
in vec3 vCenterOfMass;
in float vMaxDistance;
in vec3 vPosition;
 
void main()
{
	float t = distance(vCenterOfMass,vPosition)/vMaxDistance;
    gl_FragColor = mix(vec4(1.0,0.0,0.0,1.0), vec4(0.0,0.0,1.0,1.0), t);
}