#pragma once
#include <GLFW\glfw3.h>
#include <list>


class Sprite
{
private:
	std::list<Sprite*> children;
public:
	Sprite(){
	}
	virtual ~Sprite(){}
	virtual void Draw(){
		for each (Sprite* child in children)
		{
			child->Draw();
		}
	}
	virtual void OnEveryFrame(){
		for each (Sprite* child in children)
		{
			child->OnEveryFrame();
		}
	}

	void AddChild(Sprite * s){
		s->parent = this;
		children.push_back(s);
	}
	void RemoveChild(Sprite * s){
		s->parent = NULL;
		children.remove(s);
	}
	void RemoveAllChildren(){
		children.clear();
	}

	float x, y;
	Sprite * parent = NULL;

	void Move(float x, float y){
		for each (Sprite* child in children)
		{
			child->Move(x, y);
		}
		this->x += x;
		this->y += y;
	}
};

