#version 150
 
attribute vec3 position;
attribute float rotationAngle;

void main()
{
    gl_Position = vec4(position.x * cos(rotationAngle) - position.y * sin(rotationAngle),
						position.x * sin(rotationAngle) + position.y * cos(rotationAngle),
						position.z,
						1.0);
}