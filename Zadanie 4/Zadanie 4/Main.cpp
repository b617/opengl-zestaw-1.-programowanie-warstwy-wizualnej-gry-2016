#include "stdafx.h"
/*Standard libraries*/
#include <stdlib.h>
#include <string>
#include <vector>

/*Additional libraries*/
#include "gl3w.h"
#include <GLFW\glfw3.h>

/*Classes*/
#include "Shader.h"
#include "Mesh.h"
#include "Application.h"

#define M_PI 3.14159265358979323846
//---------------------------------------------------------
void StartGL();
void QuitGL();

int main(int argc, char* argv[])
{
	/* INIT */
	StartGL();
	Application & application = Application::GetInstance();

	float rotationAngle = 30;
	rotationAngle *= 2 * M_PI / 180;	//zamiana stopni na radiany
	Shader shader("shaders/testShader");
	GLuint shader_rotationAnglePosition = glGetAttribLocation(shader.GetProgramID(), "rotationAngle");
	glVertexAttrib1f(shader_rotationAnglePosition, rotationAngle);

	Vertex verices[] = { Vertex(glm::vec3(-.5, -.5, 0)), Vertex(glm::vec3(0, .5, 0)), Vertex(glm::vec3(.5, -.5, 0)) };
	Mesh mesh(verices, sizeof(verices) / sizeof(verices[0]));

	/* MAIN LOOP */
	while (glfwWindowShouldClose(application.window) == GL_FALSE){
		glfwGetFramebufferSize(application.window, &(application.windowSizeX), &(application.windowSizeY));
		application.windowRatio = 1.0f * application.windowSizeX / application.windowSizeY;
		glViewport(0, 0, application.windowSizeX, application.windowSizeY);
		glClear(GL_COLOR_BUFFER_BIT);

		////////////////////////////////
		rotationAngle += 0.005f;
		if (rotationAngle > 2 * M_PI) rotationAngle -= 2 * M_PI;
		glVertexAttrib1f(shader_rotationAnglePosition, rotationAngle);
		shader.Bind();
		mesh.Draw();
		///////////////////////////////

		glfwSwapBuffers(application.window);
		glfwPollEvents();
	}

	QuitGL();
	return EXIT_SUCCESS;
}

void StartGL(){
	/*GLFW*/
	if (glfwInit() == false) {
		puts("Failed to initialize glfw");
		system("pause");
		exit(EXIT_FAILURE);
	}

	Application & application = Application::GetInstance();
	if (application.CreateNewWindow() == false) {
		puts("Failed to create window");
		glfwTerminate();
		system("Pause");
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(application.window);
	glfwSetKeyCallback(application.window, key_callback);
	glfwSwapInterval(1);

	/*GL3W*/
	int iTemp = gl3wInit();
	puts("openGL version:");
	const GLubyte* version = glGetString(GL_VERSION);
	printf("%s\n", version);

	if (iTemp != 0){
		puts("Failed to initialize gl3w: ");
		fprintf(stderr, std::to_string(iTemp).c_str());
		system("pause");
		exit(EXIT_FAILURE);
	}

	glClearColor(0, 0, 0, 1);
}
void QuitGL(){
	glfwDestroyWindow(Application::GetInstance().window);
	glfwTerminate();
}