#include "stdafx.h"
/*Standard libraries*/
#include <stdlib.h>
#include <string>
#include <vector>

/*Additional libraries*/
#include "gl3w.h"
#include <GLFW\glfw3.h>

/*Classes*/
#include "Shader.h"
#include "Mesh.h"
#include "Application.h"

//---------------------------------------------------------
void StartGL();
void QuitGL();

std::vector<Vertex> vertices;
Mesh * mesh2 = NULL;
Shader * shader1;// ("shaders/wierzcholki");
Shader * shader2;// ("shaders/fragmenty");
bool shader_1_is_active = true;
Shader * activeShader = shader1;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	//std::cout << key << " " << scancode << " " << action << " " << mods <<std::endl;
	if (action == GLFW_PRESS) return;

	if (key == GLFW_KEY_ESCAPE)
		glfwSetWindowShouldClose(window, GL_TRUE);
	else if (key == GLFW_KEY_SPACE){
		if (shader_1_is_active) activeShader = shader2;
		else activeShader = shader1;
		shader_1_is_active = !shader_1_is_active;
	}
	else if (key == GLFW_KEY_DELETE){
		delete mesh2;
		vertices.clear();
		mesh2 = NULL;
		puts("\nDELETING MESH\n");
	}
}

void OnMouseClick(GLFWwindow* window, int button, int action, int mods){
	if (action == GLFW_PRESS) return;
	double x, y;
	glfwGetCursorPos(window, &x, &y);
	x = (x / Application::GetInstance().windowSizeX) * 2 - 1;
	y = 1 - (2* y / Application::GetInstance().windowSizeY);
	vertices.push_back(Vertex(glm::vec3(x, y, 0.0f)));

	printf("vertices count: %d Vertex(%s)\n", vertices.size(),vertices[vertices.size()-1].toString().c_str());

	if (vertices.size() >= 3){
		if (mesh2 != NULL) delete mesh2;
		puts("Creating object");
		mesh2 = new Mesh(&vertices[0], vertices.size(), GL_TRIANGLE_STRIP);
	}
}

int main(int argc, char* argv[])
{
	/* INIT */
	StartGL();
	Application & application = Application::GetInstance();
	
	Shader _shader1("shaders/wierzcholki");
	Shader _shader2("shaders/fragmenty");
	shader1 = &_shader1;
	shader2 = &_shader2;
	activeShader = shader1;

	/* MAIN LOOP */
	while (glfwWindowShouldClose(application.window) == GL_FALSE){
		glfwGetFramebufferSize(application.window, &(application.windowSizeX), &(application.windowSizeY));
		application.windowRatio = 1.0f * application.windowSizeX / application.windowSizeY;
		glViewport(0, 0, application.windowSizeX, application.windowSizeY);
		glClear(GL_COLOR_BUFFER_BIT);

		////////////////////////////////
		activeShader->Bind();
		if (mesh2 != NULL) mesh2->Draw();
		///////////////////////////////

		glfwSwapBuffers(application.window);
		glfwPollEvents();
	}

	QuitGL();
	return EXIT_SUCCESS;
}

void StartGL(){
	/*GLFW*/
	if (glfwInit() == false) {
		puts("Failed to initialize glfw");
		system("pause");
		exit(EXIT_FAILURE);
	}

	Application & application = Application::GetInstance();
	if (application.CreateNewWindow() == false) {
		puts("Failed to create window");
		glfwTerminate();
		system("Pause");
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(application.window);
	glfwSetKeyCallback(application.window, key_callback);

	glfwSetMouseButtonCallback(application.window, OnMouseClick);
	glfwSwapInterval(1);


	/*GL3W*/
	int iTemp = gl3wInit();
	puts("openGL version:");
	const GLubyte* version = glGetString(GL_VERSION);
	printf("%s\n", version);

	if (iTemp != 0){
		puts("Failed to initialize gl3w: ");
		fprintf(stderr, std::to_string(iTemp).c_str());
		system("pause");
		exit(EXIT_FAILURE);
	}

	glClearColor(.3, .3, .3, 1);
}
void QuitGL(){
	glfwDestroyWindow(Application::GetInstance().window);
	glfwTerminate();
}