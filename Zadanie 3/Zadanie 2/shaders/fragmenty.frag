#version 140

in vec3 position2;

void main()
{
	vec4 color;
	vec3 position=position2;
	
	if(position.x<(-0.333)){
		if(position.y>(0.333)){
			color=vec4(0.0,0.0,1.0,1.0);	//granatowy
		}
		else if(position.y<(-0.333)){
			color=vec4(1.0,0.0,0.0,1.0);	//czerwony
		}
		else{
			color=vec4(0.0,1.0,0.0,1.0); 	//zielony
		}
	}
	else if(position.x>(0.333)){
		if(position.y>(0.333)){
			color=vec4(1.0,0.0,1.0,1.0);	//magenta
		}
		else if(position.y<(-0.333)){
			color=vec4(1.0,1.0,1.0,1.0);	//bialy
		}
		else{
			color=vec4(0.5,0.5,0.5,1.0); 	//szary
		}
	}
	else{
		if(position.y>(0.333)){
			color=vec4(0.0,0.0,0.0,1.0);	//czarny
		}
		else if(position.y<(-0.333)){
			color=vec4(1.0,1.0,0.0,1.0);	//zolty
		}
		else{
			color=vec4(0.0,1.0,1.0,1.0); 	//cyan
		}
	}
    gl_FragColor = color;
}