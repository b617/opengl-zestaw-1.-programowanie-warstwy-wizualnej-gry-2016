#version 140
 
attribute vec3 position;
out vec4 color;

void main()
{
    gl_Position = vec4(position,1.0);
	
	if(position.x<(-0.333)){
		if(position.y>(0.333)){
			color=vec4(0.0,0.0,1.0,1.0);	//granatowy
		}
		else if(position.y<(-0.333)){
			color=vec4(1.0,0.0,0.0,1.0);	//czerwony
		}
		else{
			color=vec4(0.0,1.0,0.0,1.0); 	//zielony
		}
	}
	else if(position.x>(0.333)){
		if(position.y>(0.333)){
			color=vec4(1.0,0.0,1.0,1.0);	//magenta
		}
		else if(position.y<(-0.333)){
			color=vec4(1.0,1.0,1.0,1.0);	//bialy
		}
		else{
			color=vec4(0.5,0.5,0.5,1.0); 	//szary
		}
	}
	else{
		if(position.y>(0.333)){
			color=vec4(0.0,0.0,0.0,1.0);	//czarny
		}
		else if(position.y<(-0.333)){
			color=vec4(1.0,1.0,0.0,1.0);	//zolty
		}
		else{
			color=vec4(0.0,1.0,1.0,1.0); 	//cyan
		}
	}
}