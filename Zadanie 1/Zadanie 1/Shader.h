#pragma once
#include <string>
#include "gl3w.h"

class Shader
{
public:
	Shader(const std::string& fileName);
	void Bind();

	GLuint CreateShader(std::string text,GLenum shaderType);
	void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage);
	GLuint GetProgramID();

	static std::string FileToString(const std::string & fileName);
	virtual ~Shader();
private:
	static const unsigned int NUM_SHADERS = 2;

	GLuint mProgram;
	GLuint mShaders[NUM_SHADERS];
};

