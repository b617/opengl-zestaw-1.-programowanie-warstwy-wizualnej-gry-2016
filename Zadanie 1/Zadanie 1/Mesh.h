#pragma once
#include "glm\glm.hpp"
#include "gl3w.h"

class Vertex;

class Mesh
{
public:
	Mesh(Vertex* vertices, unsigned int numVertices);


	void Draw();

	Mesh & setDrawingMode(GLuint value);

	virtual ~Mesh();
private:
	enum
	{
		POSITION_VB,

		NUM_BUFFERS
	};
	GLuint mVertexArrayObject;
	GLuint mVertexArrayBuffers[NUM_BUFFERS];
	unsigned int mDrawCount;
	GLuint mDrawingMode = GL_TRIANGLES;
};

class Vertex{
public:
	Vertex(const glm::vec3& position) : pos(position){ }
protected:
	glm::vec3 pos;
};
